﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemeyMovement : MonoBehaviour
{
    gameManager GameManager;
    void Start()
    {
        GameObject gameControler = GameObject.FindGameObjectWithTag("GameController");
        GameManager = gameControler.GetComponent <gameManager>();
    }
 void Update()
    {
        transform.Translate(GameManager.moveVector * GameManager.moveSpeed *Time.deltaTime);
    }
}
